package uk.ac.reading.cs2ja16;

/**
 * Created by Owner on 20/02/2018.
 * class for the ball object
 */

public class Ball extends GameObject {

    // object also needs speed
    public float ballSpeedX;
    public float ballSpeedY;

    public Ball(){
        this.setXY(-100, -100);
        this.ballSpeedX = 0;
        this.ballSpeedY = 0;
    }

    public Ball(float x, float y, float xSpd, float ySpd){
        this.XPos = x;
        this.YPos = y;
        this.ballSpeedX = xSpd;
        this.ballSpeedY = ySpd;
    }

    public void setSpeedXY(float x, float y){
        this.ballSpeedX = x;
        this.ballSpeedY = y;
    }
}
