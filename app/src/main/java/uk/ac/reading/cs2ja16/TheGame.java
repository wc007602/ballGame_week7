package uk.ac.reading.cs2ja16;

//Other parts of the android libraries that we use
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class TheGame extends GameThread {

    //The X and Y position of the ball on the screen
    //The point is the top left corner, not middle, of the ball
    //Initially at -100 to avoid them being drawn in 0,0 of the screen
    private float mBallX = -100;
    private float mBallY = -100;

    //The speed (pixel/second) of the ball in direction X and Y
    private float mBallSpeedX = 0;
    private float mBallSpeedY = 0;

    private Ball mBall = new Ball(mBallX, mBallY, mBallSpeedX, mBallSpeedY);

    //Paddle's x position. Y will always be the bottom of the screen
    private float mPaddleX = 0;

    //The speed (pixel/second) of the paddle in direction X and Y
    private float mPaddleSpeedX = 0;

    private Paddle mPaddle = new Paddle(mPaddleX, mPaddleSpeedX);

    //The X and Y position of the ball on the screen
    //The point is the top left corner, not middle, of the ball
    //Initially at -100 to avoid them being drawn in 0,0 of the screen
    private float mSmileyBallX = -100;
    private float mSmileyBallY = -100;

    private Obstacle mSmileyBall = new Obstacle(mSmileyBallX, mSmileyBallY);

    //The X and Y position of the SadBalls on the screen
    //The point is the top left corner, not middle, of the balls
    //Initially at -100 to avoid them being drawn in 0,0 of the screen
    private float[] mSadBallX = {-100, -100, -100};
    private float[] mSadBallY = new float[3];

    private Obstacle[] mSadBall = new Obstacle[3];

    //This will store the min distance allowed between a big ball and the small ball
    //This is used to check collisions
    private float mMinDistanceBetweenBallAndPaddle = 0;

    //This is run before anything else, so we can prepare things here
    public TheGame(GameView gameView) {
        //House keeping
        super(gameView);

        //Prepare the image so we can draw it on the screen (using a canvas)
        mBall.Image = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.small_red_ball);

        //Prepare the image of the paddle so we can draw it on the screen (using a canvas)
        mPaddle.Image = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.yellow_ball);

        //Prepare the image of the SmileyBall so we can draw it on the screen (using a canvas)
        mSmileyBall.Image = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.smiley_ball);

        for(int i = 0; i < 3; i++){
            //Prepare the image of the SadBall(s) so we can draw it on the screen (using a canvas)
            mSadBall[i].Image = BitmapFactory.decodeResource
                    (gameView.getContext().getResources(),
                            R.drawable.sad_ball);
        }
    }

    //This is run before a new game (also after an old game)
    @Override
    public void setupBeginning() {
        //Initialise speeds
        //mCanvasWidth and mCanvasHeight are declared and managed elsewhere
        mBall.ballSpeedX = mCanvasWidth / 3;
        mBall.ballSpeedY = mCanvasHeight / 3;

        //Place the ball in the middle of the screen.
        //mBall.Width() and mBall.getHeigh() gives us the height and width of the image of the ball
        mBall.XPos = mCanvasWidth / 2;
        mBall.YPos = mCanvasHeight / 2;

        //Place Paddle in the middle of the screen
        mPaddle.XPos = mCanvasWidth / 2;

        //Place SmileyBall in the top middle of the screen
        mSmileyBall.XPos = mCanvasWidth / 2;
        mSmileyBall.YPos = mSmileyBall.getHeight()/2;

        //Place all SadBalls forming a pyramid underneath the SmileyBall
        mSadBall[0].XPos = mCanvasWidth / 3;
        mSadBall[0].YPos = mCanvasHeight / 3;

        mSadBall[1].XPos = mCanvasWidth - mCanvasWidth / 3;
        mSadBall[1].YPos = mCanvasHeight / 3;

        mSadBall[2].XPos = mCanvasWidth / 2;
        mSadBall[2].YPos = mCanvasHeight / 5;

        //Get the minimum distance between a small ball and a bigball
        //We leave out the square root to limit the calculations of the program
        //Remember to do that when testing the distance as well
        mMinDistanceBetweenBallAndPaddle = (mPaddle.getWidth() / 2 + mBall.getWidth() / 2) * (mPaddle.getWidth() / 2 + mBall.getWidth() / 2);
    }

    @Override
    protected void doDraw(Canvas canvas) {
        //If there isn't a canvas to do nothing
        //It is ok not understanding what is happening here
        if(canvas == null) return;

        //House keeping
        super.doDraw(canvas);

        //canvas.drawBitmap(bitmap, x, y, paint) uses top/left corner of bitmap as 0,0
        //we use 0,0 in the middle of the bitmap, so negate half of the width and height of the ball to draw the ball as expected
        //A paint of null means that we will use the image without any extra features (called Paint)

        //draw the image of the ball using the X and Y of the ball
        canvas.drawBitmap(mBall.Image, mBall.XPos - mBall.getWidth() / 2, mBall.YPos - mBall.getHeight() / 2, null);

        //Draw Paddle using X of paddle and the bottom of the screen (top/left is 0,0)
        canvas.drawBitmap(mPaddle.Image, mPaddle.XPos - mPaddle.getWidth() / 2, mCanvasHeight - mPaddle.getHeight() / 2, null);

        //Draw SmileyBall
        canvas.drawBitmap(mSmileyBall.Image, mSmileyBall.XPos - mSmileyBall.getWidth() / 2, mSmileyBall.YPos - mSmileyBall.getHeight() / 2, null);

        //Loop through all SadBall
        for(int i = 0; i < 3; i++) {
            //Draw SadBall in position i
            canvas.drawBitmap(mSadBall[i].Image, mSadBall[i].XPos - mSadBall[i].getWidth() / 2, mSadBall[i].YPos - mSadBall[i].getHeight() / 2, null);
        }
    }


    //This is run whenever the phone is touched by the user
    @Override
    protected void actionOnTouch(float x, float y) {
        //Move the ball to the x position of the touch
        mPaddle.XPos = x;
    }


    //This is run whenever the phone moves around its axises
    @Override
    protected void actionWhenPhoneMoved(float xDirection, float yDirection, float zDirection) {
        //Change the paddle speed
        mPaddle.paddleSpeedX = mPaddle.paddleSpeedX + 70f * xDirection;

        //If paddle is outside the screen and moving further away
        //Move it into the screen and set its speed to 0
        if(mPaddle.XPos <= 0 && mPaddle.paddleSpeedX < 0) {
            mPaddle.paddleSpeedX = 0;
            mPaddle.XPos = 0;
        }
        if(mPaddle.XPos >= mCanvasWidth && mPaddle.paddleSpeedX > 0) {
            mPaddle.paddleSpeedX = 0;
            mPaddle.XPos = mCanvasWidth;
        }

    }


    //This is run just before the game "scenario" is printed on the screen
    @Override
    protected void updateGame(float secondsElapsed) {
        //If the ball moves down on the screen
        if(mBall.ballSpeedY > 0) {
            //Check for a paddle collision
            updateBallCollision(mPaddle.XPos, mCanvasHeight);
        }

        //Move the ball's X and Y using the speed (pixel/sec)
        mBall.XPos = mBall.XPos + secondsElapsed * mBall.ballSpeedX;
        mBall.YPos = mBall.YPos + secondsElapsed * mBall.ballSpeedY;

        //Move the paddle's X and Y using the speed (pixel/sec)
        mPaddle.XPos = mPaddle.XPos + secondsElapsed * mPaddle.paddleSpeedX;


        //Check if the ball hits either the left side or the right side of the screen
        //But only do something if the ball is moving towards that side of the screen
        //If it does that => change the direction of the ball in the X direction
        if((mBall.XPos <= mBall.getWidth() / 2 && mBall.ballSpeedX < 0) || (mBall.XPos >= mCanvasWidth - mBall.getWidth() / 2 && mBall.ballSpeedX > 0) ) {
            mBall.ballSpeedX = -mBall.ballSpeedX;
        }

        //Check for SmileyBall collision
        if(updateBallCollision(mSmileyBall.XPos, mSmileyBall.YPos)) {
            //Increase score
            updateScore(1);
        }

        //Loop through all SadBalls
        for(int i = 0; i < mSadBallX.length; i++) {
            //Perform collisions (if necessary) between SadBall in position i and the red ball
            updateBallCollision(mSadBallX[i], mSadBallY[i]);
        }

        //If the ball goes out of the top of the screen and moves towards the top of the screen =>
        //change the direction of the ball in the Y direction
        if(mBall.YPos <= mBall.getWidth() / 2 && mBall.ballSpeedY < 0) {
            mBall.ballSpeedY = -mBall.ballSpeedY;
        }

        //If the ball goes out of the bottom of the screen => lose the game
        if(mBall.YPos >= mCanvasHeight) {
            setState(GameThread.STATE_LOSE);
        }

    }

    //Collision control between mBall and another big ball
    private boolean updateBallCollision(float x, float y) {
        //Get actual distance (without square root - remember?) between the mBall and the ball being checked
        float distanceBetweenBallAndPaddle = (x - mBall.XPos) * (x - mBall.XPos) + (y - mBall.YPos) *(y - mBall.YPos);

        //Check if the actual distance is lower than the allowed => collision
        if(mMinDistanceBetweenBallAndPaddle >= distanceBetweenBallAndPaddle) {
            //Get the present speed (this should also be the speed going away after the collision)
            float speedOfBall = (float) Math.sqrt(mBall.ballSpeedX*mBall.ballSpeedX + mBall.ballSpeedY*mBall.ballSpeedY);

            //Change the direction of the ball
            mBall.ballSpeedX = mBall.XPos - x;
            mBall.ballSpeedY = mBall.YPos - y;

            //Get the speed after the collision
            float newSpeedOfBall = (float) Math.sqrt(mBall.ballSpeedX*mBall.ballSpeedX + mBall.ballSpeedY*mBall.ballSpeedY);

            //using the fraction between the original speed and present speed to calculate the needed
            //velocities in X and Y to get the original speed but with the new angle.
            mBall.ballSpeedX = mBall.ballSpeedX * speedOfBall / newSpeedOfBall;
            mBall.ballSpeedY = mBall.ballSpeedY * speedOfBall / newSpeedOfBall;

            return true;
        }

        return false;
    }
}

// This file is part of the course "Begin Programming: Build your first mobile game" from futurelearn.com
// Copyright: University of Reading and Karsten Lundqvist
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
//
// You should have received a copy of the GNU General Public License
// along with it.  If not, see <http://www.gnu.org/licenses/>.
