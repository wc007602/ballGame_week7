package uk.ac.reading.cs2ja16;

import android.graphics.Bitmap;

/**
 * Created by Owner on 20/02/2018.
 * GameObject class is abstract class that is not instantiated
 * All other objects will inherit from this
 */

public abstract class GameObject {

    // all variables each object should have
    protected float XPos;        // gameObject X position
    protected float YPos;        // gameObject Y pos

    protected Bitmap Image;   // image to be displayed in thread

    /**
     * method sets x and y pos for the game object
     * @param x
     * @param y
     */
    protected void setXY(float x, float y){
        this.XPos = x;
        this.YPos = y;
    }

    public float getWidth(){
        return this.Image.getWidth();
    }

    public float getHeight(){
        return this.Image.getHeight();
    }

}
