package uk.ac.reading.cs2ja16;

import android.graphics.BitmapFactory;

/**
 * Created by Owner on 20/02/2018.
 */

public class Obstacle extends GameObject {

    /**
     * default construct
     */
    public Obstacle(){
        this.setXY(-100, -100);         // draw below canvas so will not show onCreate
    }

    public Obstacle(float x, float y){
        this.XPos = x;
        this.YPos = y;
    }
}
