package uk.ac.reading.cs2ja16;

/**
 * Created by Owner on 20/02/2018.
 * Paddle class controls the player (paddle)
 */

public class Paddle extends GameObject {

    public float paddleSpeedX;

    public Paddle(){
        this.setXY(0, 0);
        this.paddleSpeedX = 0;
    }

    public Paddle (float x, float xSpd){
        this.XPos = x;
        this.paddleSpeedX = xSpd;
    }

    public void setX(float x){
        this.XPos = x;
    }

}
